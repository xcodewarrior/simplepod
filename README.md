# SimplePod

[![CI Status](https://img.shields.io/travis/eliakorkmaz/SimplePod.svg?style=flat)](https://travis-ci.org/eliakorkmaz/SimplePod)
[![Version](https://img.shields.io/cocoapods/v/SimplePod.svg?style=flat)](https://cocoapods.org/pods/SimplePod)
[![License](https://img.shields.io/cocoapods/l/SimplePod.svg?style=flat)](https://cocoapods.org/pods/SimplePod)
[![Platform](https://img.shields.io/cocoapods/p/SimplePod.svg?style=flat)](https://cocoapods.org/pods/SimplePod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SimplePod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SimplePod'
```

## Author

eliakorkmaz, emrahkrkmz1@gmail.com

## License

SimplePod is available under the MIT license. See the LICENSE file for more info.
